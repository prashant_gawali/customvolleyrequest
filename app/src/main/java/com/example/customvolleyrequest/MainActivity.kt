package com.example.customvolleyrequest

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val queue = Volley.newRequestQueue(this)
        val url = "url"

        val sr: StringRequest = object : StringRequest(Request.Method.GET, url,
                Response.Listener<String?> { response ->
                    Log.e("HttpClient", "success! response: $response")
                },
                Response.ErrorListener { error ->
                    Log.e("HttpClient", "error: $error")
                }) {

            override fun getParams(): Map<String, String>? {
                val params: MutableMap<String, String> = HashMap()
                params["user"] = "name"
                params["pass"] = "password"
                return params
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/x-www-form-urlencoded"
                return params
            }
        }

        queue.add(sr)
    }
}